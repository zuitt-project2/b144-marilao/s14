// alert('working')

//Functions


function printStar(){
	console.log("*")
	console.log("**")
	console.log("***")
}

function sayHello(){
	console.log('Hello')
}

/*function alertPrint(){
	alert('hello');
	console.log('hello');
}
alertPrint();*/

function sayHello(name){
	
	console.log("Hello" + name)
}
sayHello(6)//concatenation

//Function that accepts two numbers
//and prints the sum

//Parameters

function addSum(x,y)
/*function addSum()*/{
	/*let sum= 10 + 5*/
	let sum = x + y
	console.log(sum)
}
/*addSum()
addSum()*/
addSum(13,2)//arguments
addSum(23,56)

//Function with 3 parameters
//String Tempalte Literals
//Interpolation
function printBio(lname, fname, age){
	// console.log("Hello" + lname + fname + age)
	console.log(`hello Mr. ${lname} ${fname} ${age}`)
}

printBio('Stark','Tony',50)


//Return keyword
function addSum(x,y){
	return y-x
	console.log(x + y)
}
let sumNum = addSum(3,4)
// addSum(3,4)