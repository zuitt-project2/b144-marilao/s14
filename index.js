/*Writing Comment in Javascript*/
// one-line comments ctrl + / 
/*multi-line comment - ctrl + shift + / */
console.log("Hello Batch 144!");

/*

	Javascript - we can see or log messages in our console.

	Browser Consoles are part of our browsers which will allow us see/log messages, data or info from our programming language - JS.

	For most browsers, consoles are easily accessed through its developer tools in the console tab.

	In fact, consoles is browsers will allow us to add some JS expressions.

	Statements

	Statements are instructions, expressions that we add to our programming language to communicate with our computers.

	JS Statements usually ends in a semi colon(;), However, JS has implemented a way to automatically add semicolons at the end of a line/statement. Semi Colons can actually be omitted in creating JS Statements but semicolons in JS are added to mark the end of the statements.

	Syntax

	In programming is a set of rules that describes how statements are properly made/constructed.

	Lines/blocks of code must follow a certain of rules for it to work properly.


*/
console.log("Shiela Rose Marilao");

/*Mini Activity

	Create three console logs to display the following message:

		console.log("<yourFavouriteFood>")

	

*/
/*console.log("Kalderetang Kambing");
console.log("Kalderetang Kambing");
console.log("Kalderetang Kambing");*/

let food1 = "Kalderetang Kambing";

console.log(food1);


/*

	Variables are a way to store info or data within our JS.

	To create a variable we first declare the name of the variable with either the let/const keword:

	let nameOfVariable

	Then, we can initialize the variable with a value or data.

	let nameOfVariable

	Then, we can initialize the variable with a value or data.

	let nameOfVariable = (data);

*/
console.log("My favourite food is: " + food1)

let food2 = "Chicken Adobo";
console.log("My favourite foods are: " + food1 + " and " + food2)

let food3;

/*

	We can create variables without add an initial values, however, the variable's content is undefined.

*/

console.log(food3);

food3 = "Chickenjoy";

/*

	We can update the content of a let variable by reassigning the content using an assignment operator (=);

	assignment operator (=) = let us assign data to a variable.
*/

console.log(food3);

/ Mini-Activity /

 food1 = "Sinigang na Salmon";
 food2 = "Munggo";
console.log(food1);
console.log(food2);

/*

	We can update our variables with an assignment operator without needing to use the let keyword again.

	We cannot create another variable with the same time. It will result in an error.

*/
// let food1 = "Flat Tops";

// const keyword
/*

	const keyword will also allow us to create variables. However, with a const keyword we create constant variables, which means these data to saved in a constant will not change, cannot be changed and should not be changed.

*/

const pi = 3.1416
console.log(pi);


// Trying to re-assign a const var will result into an error.

/*pi = "Pizza"

console.log(pi)*/

/*const gravity;

console.log(gravity);*/

//We also cannot declare/create a const var without initialization or an initial value.

// Mini-Activity

let myName;
const sunriseDirection = "East";
const sunsetDirection = "West";
console.log(myName);
console.log(sunriseDirection);
console.log(sunsetDirection);
// or you can log multiple items, var,data in console.log separated by , console.log(myName,sunriseDirection,sunsetDirection);

/*

	Guides in creating a JS variable:

	1. We can create a let var with the let keyword. let var can be reassigned but not declared.

	2. Creating a var has two parts: Declaration of the var name and Initialization of the initian value of the var using an assignment operator (=)

	3. A let var can be declared without initialization. However, the value of the variable will be undefined until it is re-assigned with a value.

	4. Not defined vs Undefined. Not Defined error means the variable is used but NOT declared. Undefined results from a variable thatis used but is not initialized.

	5. We can use const keyword to create constant variables. Constant variables can be declared without initialization. Constant variables cannot be re-assigned.

	6. When creating variable names, start with small caps, this is because of avoiding conflict with syntax in JS that is named with capital letters.

	7. If the variable would need two words, the naming convention is the use of camelCase. Do not add a space fr var with words as names.

*/

//Data types
/*

	Strings are data which are alphanumerical text. It could be a name, a phrase or even a sentence. We can create string with single quote('') or with a double quote (" ").

	variable = "string data"
*/
console.log("Sample String Data");

let country = "Philippines";
let province = "Rizal";

console.log(province,country);

/*

	Concatenation - is a way to add strings together and have a string. We can use our "+" symbol for this.

*/
let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting = "I live in " + country;
console.log(greeting);

/*

	In JS, when you use the + sign with strings we have concatenation.
*/

let numString = "50";
let numString2 = "25";

console.log(numString + numString2);

/*Strings have property calle .length abd it tells us the number of characters in a string.

Spaces, Commas, Periods can also be characters when included in a string.

It will return a number type data.
*/

let hero = "Captain America";

console.log(hero.length);

/*Number Type*/
/*

	Number Type data can actually be used in proper mathematical equations and operations.

*/
let students = 16;
console.log(students);

let num1 = 50;
let num2 = 25;


/*Addition Operator will allow us to add two number typesand return the sum as a number data type.

Operations return a value and that value can be saved in a variable.

What if we use the addition operator on a number type and a numerical string? It results to concatenation.

parseInt will allow us to turn a numeric string into a proper number type.

*/

let sum1 = num1 + num2

console.log(sum1);

let numString3 = "100";

let sum2 = parseInt(numString3) + num1
console.log(sum2);

const name = "Jeff";
console.log(num1,numString3);

/* Mini-Activity*/

let sum3 = sum1 + sum2
let sum4 = parseInt(numString2) + num2
console.log(sum3,sum4);

/*

	Subtraction Operator

	Will let us get the difference between two number types. It will also resultto a proper number type data.

	When subtraction operator is used on a string and a number, the string will be converted into a number automatically and then JS will perform the operation.

	This automatic conversion from one type to another is called Type Conversion or Type Coercion or Forced Coercion.


	When a text string is subtracted with a number, it will result in NaN or Not a Number because when JS converted the text string it results to NaN and NaN-number = NaN
*/
let difference = num1 - num2;
console.log(difference);

let difference2 = numString3 - num2;
console.log(difference2);

let difference3 = hero - num2;
console.log(difference3);

let string1 = "fifteen";

console.log(num2 - string1);

/*Multiplication Operator(*)*/

let num3 = 10;
let num4 = 5;
let product = num3*num4
console.log(product);
let product2 = numString3 * num3
console.log(product2);
let product3 = numString * numString3
console.log(product3);

/*Division Operator (/)*/
let num5 = 30;
let num6 = 3;
let quotient = num5/num6;
console.log(quotient);
let quotient2 = numString3/5;
console.log(quotient2)
let quotient3 = 450/num4;
console.log(quotient3);

/* Boolean (true or false)*/
/* used for logical operations or for if-else conditions.*/
/*Naming convetion for a variable that contains boolean is a yes or no question.*/

let isAdmin = true;
let isMarried = false;

/* Variable must be:

1.appropriate
2.definitive of the value it contains.
semantically correct
*/

//Arrays
/*
	Special kind of data type wherein we can store multpiple values.
	Can store multiple values and even of different types. For best practice, keep the data type of items in an array uniform.
	Values in an array are separate by comma. Faling to do so, will result in an error.
	An array is created with an Array Literal ([])
	
	Array indices are markers of the order of the items in the array. Array index starts at 0.

*/
let koponanNiEugene = ["Eugene","Alfred","Dennis","Vincent"];
console.log(koponanNiEugene);
//To access an array item : arrayName[index]
console.log(koponanNiEugene[0]);
//bad practice for an array:
let array2 =  ["One Punch Man","true",500,"Saitama"];
console.log(array2);

//Objects
/*
	Another kind of special data type used to mimic real world objects.
	With this we can add infos. that makes sense, thematically relevant, and of different data types.
	Objects can be created with Objects Literals({})
	Each value is given a label which ,akes the data significant and meaningful this pairing of data and "label" is what we call a Key-Value pair


*/
let person1 = {

	heroName: "One Punch Man",
	isRegistered: true,
	salary: 500,
	realName: "Saitama"
};

//To get the value of an object's property, we can access it using dot notation.
//objectName.propertyName
console.log(person1.realName);

//Mini-activity

let myFavoriteBands = ["MYMP","A1","Backstreetboys","Moffats"];
// console.log(myFavoriteBands);

let me = {
	firstName : "Shiela Rose",
	lastName : "Marilao",
	isWebDeveloper : true,
	hasPortfolio : true,
	age : 31
};
// console.log(me.firstName);

//Undefined vs Null

/*Null is the explicit declaration that means there is no value.*/

let sampleNull = null;

/*Undefined means that the variable exists however a value was not initialized with the variable.*/

let undefinedSample;
console.log(undefinedSample);

//Certain processes in programming explicitly returns null to indicate that the task resulted to nothing.

let foundResult = null;

//For undefined, this is notmally caused by developers creating variables that have no value or data associated with them.
//The variable does exist but its value is still unknown.

let person2 = {
	name: "Patricia",
	age: 28
}

//because the var person2 does exist however the property isAdmin does not
console.log(person2.isAdmin);